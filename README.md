# condition-bamboo

## Installation

To install this plugin, execute:

```shell
npm install --save-dev semantic-release-condition-bamboo
```

## Usage

To use Bamboo for the CI, make the following changes:

* Configure `release` in your `package.json` as follows (showing `master` as the branch, but you can change that):

```
   "release": {
     "branch": "master",
     "verifyConditions": {
       "path": "./node_modules/semantic-release-condition-bamboo"
     }
   }
```