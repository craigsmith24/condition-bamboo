var SRError = require('@semantic-release/error')

module.exports = function (pluginConfig, config, cb) {
  var env = config.env
  var options = config.options

  if (!env.hasOwnProperty('bamboo_agentId')) {
    return cb(new SRError(
      'semantic-release didn’t run on Bamboo and therefore a new version won’t be published.\n' +
      'You can customize this behavior using "verifyConditions" plugins: git.io/sr-plugins',
      'ENOBAMBOO'
    ))
  }

  if (options.branch !== env.bamboo_planRepository_branchName) {
    return cb(new SRError(
      'This test run was triggered on the branch ' + env.bamboo_planRepository_branchName +
      ', while semantic-release is configured to only publish from ' +
      options.branch + '.\n' +
      'You can customize this behavior using the "branch" option: git.io/sr-options',
      'EBRANCHMISMATCH'
    ))
  }

  cb(null)
}